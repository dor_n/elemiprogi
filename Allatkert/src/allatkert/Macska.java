package allatkert;

import java.util.Random;

public class Macska extends Allat{
    
    public static final String OROSZLAN_HU = "Oroszlán";
    public static final String OROSZLAN_EN = "Lion";
    
    
    
    private Boolean vad;
    private String szin;
    private String fajta;

    public Macska() {
        super(0, "Új Macska");
        this.vad = Boolean.TRUE;
    }

    public Macska(String nev, Integer kor, String szin, String fajta) {
        super(kor, nev);
        Random rnd = new Random();
        this.vad = rnd.nextBoolean();
        this.szin = szin;
        this.fajta = fajta;
    }

    public Boolean isVad() {
        return this.vad;
    }

    /*
    public void setVad(Boolean vad){
        this.vad = vad;
    }
    */

    public String getSzin() {
        return szin;
    }

    public String getFajta() {
        return fajta;
    }
    
    
    public void szelidul(){
        this.vad = Boolean.FALSE;
    }
    
    public void elvadul(){
        this.vad = Boolean.TRUE;
    }
    
    public void nyavog(){
        System.out.println("Miau-Miau (" + this.nev + ")");
    }
    
    
    
    
    
    
    
    
}
